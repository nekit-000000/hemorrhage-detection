library(shiny)
library(imager)

setContrast <- function(im, vMin) {
  imgMin = min(im)
  imgMax = max(im)  
  delta = imgMax - imgMin;        
  iVMin = delta * vMin + imgMin;
  iVMax = delta * 1 + imgMin;
  iDelta = 1 / (iVMax - iVMin)
  
  newIm = (im - iVMin) * iDelta
  newIm[newIm < 0] = 0
  
  return(newIm)
}


ui <- pageWithSidebar(
  headerPanel("Hemmorhage detection"),
  sidebarPanel(
    fileInput("file", "Upload image", accept="image/png"),
    sliderInput("threshold", "Threshold:", min=0, max=1,  value=1),
    sliderInput("contrast", "Contrast:", min=0, max=1,  value=0)
  ),
  
  mainPanel(
    imageOutput("image")
  )
)

server <- function(input, output, session) {
  output$image <- renderImage({
    outfile <- tempfile(fileext = '.png')
    inFile <- input$file
    image <- NULL
    if (is.null(inFile)){
      image <- load.image("C:\\Users\\810200\\Desktop\\Diplom\\png_images\\00e9e5b2a.png")
    } else {
      image <- load.image(inFile$datapath)
    }
    
    image <- grayscale(as.cimg(image[,,,-4]))
    image[image > input$threshold] <- 0
    image <- setContrast(image, input$contrast)
    
    png(outfile, width=512, height=512)
    cscale <- function(v) rgb(v,v,v)
    image %>% plot(colourscale=cscale,rescale=FALSE)
    dev.off()
    
    list(src = outfile,
         contentType = 'image/png',
         width = 512,
         height = 512)
  }, deleteFile=T)
}

shinyApp(ui = ui, server = server)

